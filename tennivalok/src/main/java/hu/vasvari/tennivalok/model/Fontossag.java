package hu.vasvari.tennivalok.model;

public enum Fontossag {

  FONTOS,
  NORMAL,
  NEM_FONTOS;
  
}
