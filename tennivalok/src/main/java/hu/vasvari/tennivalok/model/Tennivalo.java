package hu.vasvari.tennivalok.model;

import java.time.LocalDate;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

public class Tennivalo {

  @Size(max = 36)
  private String id;

  @NotBlank
  @Size(max = 64)
  private String fejlec;

  @NotBlank
  @Size(max = 20000)
  private String leiras;

  private boolean elkeszult;

  @NotNull
  private Fontossag fontossag = Fontossag.NORMAL;

  @PastOrPresent
  private LocalDate letrehozasDatuma;

  @PastOrPresent
  private LocalDate modositasDatuma;

  // dátumformátum a böngésző formátuma szerint: pl. 2019-03-18
  @DateTimeFormat(iso = ISO.DATE)
  private LocalDate hatarido;

  public LocalDate getLetrehozasDatuma() {
    return letrehozasDatuma;
  }

  public void setLetrehozasDatuma(LocalDate letrehozasDatuma) {
    this.letrehozasDatuma = letrehozasDatuma;
  }

  public LocalDate getModositasDatuma() {
    return modositasDatuma;
  }

  public void setModositasDatuma(LocalDate modositasDatuma) {
    this.modositasDatuma = modositasDatuma;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getFejlec() {
    return fejlec;
  }

  public void setFejlec(String fejlec) {
    this.fejlec = fejlec;
  }

  public String getLeiras() {
    return leiras;
  }

  public void setLeiras(String leiras) {
    this.leiras = leiras;
  }

  public boolean isElkeszult() {
    return elkeszult;
  }

  public void setElkeszult(boolean elkeszult) {
    this.elkeszult = elkeszult;
  }

  public Fontossag getFontossag() {
    return fontossag;
  }

  public void setFontossag(Fontossag fontossag) {
    this.fontossag = fontossag;
  }

  public LocalDate getHatarido() {
    return hatarido;
  }

  public void setHatarido(LocalDate hatarido) {
    this.hatarido = hatarido;
  }

  @Override
  public String toString() {
    return "Tennivalo [id=" + id + ", fejlec=" + fejlec + ", leiras=" + leiras + ", elkeszult=" + elkeszult + "]";
  }

}
