package hu.vasvari.tennivalok.ws;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import hu.vasvari.tennivalok.model.Tennivalo;
import hu.vasvari.tennivalok.service.KeszTennivaloMarNemModosithatoException;
import hu.vasvari.tennivalok.service.TennivaloSzolgaltatas;

@RestController
public class TennivaloSzerkesztoWebService {

  @Autowired
  private TennivaloSzolgaltatas service;

  @GetMapping(value = "/tennivalo/{tennivaloAzonosito}/", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Tennivalo> tennivalotLetrehoz(@NotBlank @PathVariable String tennivaloAzonosito) {

    Tennivalo t = service.lekerdez(tennivaloAzonosito);

    return new ResponseEntity<Tennivalo>(t, HttpStatus.OK);

  }

  @PostMapping(value = "/tennivalo/", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Tennivalo> tennivalotLetrehoz(@Valid @RequestBody Tennivalo t, BindingResult bindingResult) {

    if (bindingResult.hasErrors() || t.getId() != null && !"".equals(t.getId())) {
      return new ResponseEntity<Tennivalo>(HttpStatus.BAD_REQUEST);
    }

    Tennivalo letrehozottTennivalo = service.letrehoz(t);

    return new ResponseEntity<Tennivalo>(letrehozottTennivalo, HttpStatus.CREATED);
  }

  @PutMapping(value = "/tennivalo/", consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Void> tennivalotModosit(@Valid @RequestBody Tennivalo t, BindingResult bindingResult) {

    if (bindingResult.hasErrors()) {
      return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
    }

    if (t.getId() == null) {
      return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
    }

    service.modosit(t);

    return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
  }

  @DeleteMapping(value = "/tennivalo/{tennivaloAzonosito}/")
  public ResponseEntity<Void> tennivalotModosit(@NotBlank @PathVariable String tennivaloAzonosito) {

    service.torol(tennivaloAzonosito);
    return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
  }

  @ExceptionHandler
  @ResponseStatus(value = HttpStatus.NOT_FOUND)
  public void nincsMegATennivaloHibatLekezel(EmptyResultDataAccessException e) {
  }

  @ExceptionHandler
  @ResponseStatus(value = HttpStatus.CONFLICT)
  public void nincsMegATennivaloHibatLekezel(KeszTennivaloMarNemModosithatoException e) {
  }

}
