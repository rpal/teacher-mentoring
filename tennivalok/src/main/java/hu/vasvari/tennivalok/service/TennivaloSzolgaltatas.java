package hu.vasvari.tennivalok.service;

import hu.vasvari.tennivalok.model.Tennivalo;
import hu.vasvari.tennivalok.repository.TennivaloAdatraktar;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

@Service
@Validated
public class TennivaloSzolgaltatas {

  @Autowired
  private TennivaloAdatraktar repo;

  @NotNull
  @Valid
  public Tennivalo letrehoz(@NotNull @Valid Tennivalo t) {
    // egy új véletlen azonosítót rendelünk az új tennivalóhoz:
    t.setId(UUID.randomUUID().toString());

    t.setLetrehozasDatuma(LocalDate.now());
    t.setModositasDatuma(LocalDate.now());
    repo.letrehoz(t);

    return t;
  }

  @NotNull
  public List<Tennivalo> nemElkeszulteketVisszaad() {
    return repo.visszaadjaHaElkeszultEgyenlo(false);
  }

  @NotNull
  @Valid
  public Tennivalo lekerdez(@NotBlank String id) {
    return repo.idAlapjaLeker(id);
  }

  @Transactional
  public void modosit(@NotNull @Valid Tennivalo t) {

    // ha már készre lett korábban állítva, akkor ne lehessen módosítani:
    Tennivalo adatbazisbeli = repo.idAlapjaLeker(t.getId());
    if (adatbazisbeli.isElkeszult()) {
      throw new KeszTennivaloMarNemModosithatoException();
    }

    t.setModositasDatuma(LocalDate.now());
    repo.modosit(t);
  }

  @Transactional
  public void torol(@NotBlank String id) {
    // ha már készre lett korábban állítva, akkor ne lehessen törölni:
    Tennivalo adatbazisbeli = repo.idAlapjaLeker(id);
    if (adatbazisbeli.isElkeszult()) {
      throw new KeszTennivaloMarNemModosithatoException();
    }

    repo.torolIdAlapjan(id);
  }

  @Transactional
  public void keszreAllit(@NotBlank String id) {
    Tennivalo t = repo.idAlapjaLeker(id);

    // TODO: ha már eleve kész volt a tennivaló, akkor ne lehessen
    // "újra" készre állítani
    t.setElkeszult(true);
    repo.modosit(t);
  }

}
