package hu.vasvari.tennivalok;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TennivalokApplication {

	public static void main(String[] args) {
		SpringApplication.run(TennivalokApplication.class, args);
	}

}
