package hu.vasvari.tennivalok.repository;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import hu.vasvari.tennivalok.model.Fontossag;
import hu.vasvari.tennivalok.model.Tennivalo;

@Component
public class TennivaloRekordbolObjektumraLekepezo implements RowMapper<Tennivalo> { 

  public Tennivalo mapRow(ResultSet rs, int rowNum) throws SQLException {
    
    Tennivalo t = new Tennivalo();
    
    t.setId(rs.getString("id"));
    t.setFejlec(rs.getString("fejlec"));
    t.setLeiras(rs.getString("leiras"));
    t.setElkeszult(rs.getBoolean("elkeszult"));
    t.setLetrehozasDatuma(rs.getDate("letrehozasDatuma").toLocalDate());
    t.setModositasDatuma(rs.getDate("modositasDatuma").toLocalDate());
    Date hatarido = rs.getDate("hatarido");
    t.setHatarido(hatarido != null ? hatarido.toLocalDate() : null);
    t.setFontossag(Enum.valueOf(Fontossag.class, rs.getString("fontossag")));
    return t;
  }
};