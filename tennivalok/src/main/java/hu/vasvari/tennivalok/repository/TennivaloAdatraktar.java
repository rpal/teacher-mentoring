package hu.vasvari.tennivalok.repository;

import hu.vasvari.tennivalok.model.Tennivalo;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.validation.annotation.Validated;

import static org.springframework.util.Assert.*;

@Repository
@Validated
public class TennivaloAdatraktar {

  @Autowired
  private JdbcTemplate jdbcTemplate;

  @Autowired
  private TennivaloRekordbolObjektumraLekepezo lekepezo;

  @NotNull
  public List<Tennivalo> mindetLekerdez() {

    return jdbcTemplate.query("SELECT * FROM Tennivalok", new Object[] {}, lekepezo);
  }

  @NotNull
  public List<Tennivalo> visszaadjaHaElkeszultEgyenlo(boolean elkeszult) {
    return jdbcTemplate.query("SELECT * FROM Tennivalok WHERE elkeszult=?", new Object[] { elkeszult }, lekepezo);
  }

  public void letrehoz(@NotNull @Valid Tennivalo t) {

    jdbcTemplate
        .update(
            "INSERT INTO Tennivalok (id, fejlec, leiras, elkeszult, letrehozasDatuma, modositasDatuma, fontossag, hatarido) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
            t.getId(), t.getFejlec(), t.getLeiras(), t.isElkeszult(), t.getLetrehozasDatuma(), t.getModositasDatuma(), t.getFontossag().name(), t.getHatarido());
  }

  @NotNull
  @Valid
  public Tennivalo idAlapjaLeker(@NotBlank String id) {

    Tennivalo e = jdbcTemplate.queryForObject("SELECT * FROM Tennivalok where id = ?", new Object[] { id }, lekepezo);

    if (e == null) {
      throw new EmptyResultDataAccessException(1);
    }
    return e;
  }

  public void modosit(@NotNull @Valid Tennivalo t) {
    jdbcTemplate.update("UPDATE Tennivalok set fejlec=?, leiras=?, elkeszult=?, modositasDatuma=?, fontossag=?, hatarido=? where id = ?",
        t.getFejlec(), t.getLeiras(), t.isElkeszult(), t.getModositasDatuma(), t.getFontossag().name(), t.getHatarido(), t.getId());
  }

  public void torolIdAlapjan(@NotBlank String id) {
    jdbcTemplate.update("DELETE FROM Tennivalok where id = ?", id);
  }
}
