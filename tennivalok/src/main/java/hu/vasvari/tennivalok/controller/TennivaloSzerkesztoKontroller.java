package hu.vasvari.tennivalok.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import hu.vasvari.tennivalok.model.Fontossag;
import hu.vasvari.tennivalok.model.Tennivalo;
import hu.vasvari.tennivalok.service.KeszTennivaloMarNemModosithatoException;
import hu.vasvari.tennivalok.service.TennivaloSzolgaltatas;

@Controller
public class TennivaloSzerkesztoKontroller {

  @Autowired
  private TennivaloSzolgaltatas service;

  // automatikusan bepakolja minden request mapping megtörténtekor 
  // a modellbe a fontosságok lehetséges értékeit:
  @ModelAttribute("fontossagErtekek")
  public Fontossag[] fontossagErtekekAModellbe() {
    return Fontossag.values();
  }
  
  @GetMapping("/tennivalo/")
  public ModelAndView ujTennivalohozUresUrlapotMegjelenit(@RequestParam(required = false) String ajax) {
    
    ModelAndView mav = new ModelAndView(ajax == null ? "tennivalo" : "tennivalo-ajax");
    
    if (ajax == null) {
      Tennivalo t = new Tennivalo();
      mav.addObject("tennivalo", t);
      mav.addObject("uj", true);
    }
    return mav;
  }

  @GetMapping("/tennivalo/{id}/")
  public ModelAndView tennivaloUrlapjatMegjelenit(@PathVariable String id,
      @RequestParam(required = false) String ajax) {
    
    ModelAndView mav = new ModelAndView(ajax == null ? "tennivalo" : "tennivalo-ajax");
    if (ajax == null) {
      Tennivalo t = service.lekerdez(id);
      mav.addObject("tennivalo", t);
      mav.addObject("uj", false);
    }
    return mav;
  }

  @PostMapping(value = "/tennivalo/", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
  public String tennivalotLetrehozVagyModosit(@Valid @ModelAttribute("tennivalo") Tennivalo t, //
      BindingResult bindingResult, // a BindingResult-nak mindig a validált
      // modellt kell követnie az
      // argumentumlistában
      @RequestParam(required = false) String akcio, //
      Model model) {

    // ellenőrizzük, hogy minden kötelező mező kitöltésre került-e
    // és hiba esetén informálni a felhasználót
    if (bindingResult.hasErrors() && !"torol".equals(akcio)) {
      // a mezőkhöz kapcsolódó validációs hibákat a Spring MVC eleve hozzáadta a
      // bindingResult-hoz.
      // azért pluszban még megjelenítünk egy globális (nem mezőhöz kötődő hibát
      // is a hangsúlyozás
      // kedvéért:
      bindingResult.reject("urlap.hianyos-kitoltes");

      // a felhasználót visszavezetjük az űrlapra, hogy korrigáljon:
      model.addAttribute("uj", t.getId() == null || t.getId().isEmpty());
      return "tennivalo";
    }

    // végrehajtjuk a felhasználó által kért létrehozási/mentési/törlési
    // műveletet:
    if (t.getId() == null || t.getId().isEmpty()) {

      service.letrehoz(t);

    } else {

      if ("torol".equals(akcio)) {
        service.torol(t.getId());
      } else {

        try {
          service.modosit(t);
        } catch (KeszTennivaloMarNemModosithatoException marKesz) {

          // egy globális hiba megjelenítésével tájékoztatjuk a felhasználót,
          // hogy a módosítás nem lehetséges:
          bindingResult.reject("urlap.mar-kesz-tennivalo-nem-modosithato");

          // a felhasználót visszavezetjük az űrlapra:
          return "tennivalo";
        }
      }

    }
    return "redirect:/";
  }

  @ExceptionHandler
  @ResponseStatus(value = HttpStatus.NOT_FOUND)
  public String nincsMegATennivaloHibatLekezel(EmptyResultDataAccessException e) {
    return "hiba-tennivalo-nem-talalhato";
  }

}
