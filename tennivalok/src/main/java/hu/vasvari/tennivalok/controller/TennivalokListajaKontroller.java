package hu.vasvari.tennivalok.controller;

import hu.vasvari.tennivalok.model.Tennivalo;
import hu.vasvari.tennivalok.service.TennivaloSzolgaltatas;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class TennivalokListajaKontroller {

  @Autowired
  private TennivaloSzolgaltatas service;

  @GetMapping("/")
  public ModelAndView tennivalokListaja() {

    List<Tennivalo> tennivalok = service.nemElkeszulteketVisszaad();

    ModelAndView mav = new ModelAndView("tennivalok-listaja");
    mav.addObject("tennivaloLista", tennivalok);

    return mav;
  }

  @PostMapping("/tennivalo/{id}/keszreAllit")
  public String keszreAllit(@PathVariable String id) {
    service.keszreAllit(id);
    return "redirect:/";
  }
  
  @ExceptionHandler
  @ResponseStatus(value=HttpStatus.NOT_FOUND)                          
  public String nincsMegATennivaloHibatLekezel(EmptyResultDataAccessException e) {
      return "hiba-tennivalo-nem-talalhato";           
  }
 
}
