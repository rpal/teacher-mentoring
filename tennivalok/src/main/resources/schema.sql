CREATE TABLE IF NOT EXISTS tennivalok (
  id VARCHAR(36) PRIMARY KEY,
  fejlec VARCHAR(64) NOT NULL,
  leiras CLOB,
  elkeszult BOOLEAN DEFAULT FALSE,
  fontossag VARCHAR(10) NOT NULL,
  letrehozasDatuma DATE NOT NULL,
  modositasDatuma DATE NOT NULL,
  hatarido DATE
);

CREATE INDEX IF NOT EXISTS ternnivalok_letrehozasDatuma ON tennivalok (letrehozasDatuma);
