"use strict";

(function() {

  // a tennivaló azonosítója, amivel a form dolgozik
  // (null, ha új tennivalóról van szó)
  var tennivaloId = null;

  // a dokumentum betöltődését követően inicializáljuk a form-ot...:
  $(document).ready(function() {

    // előszedjük az url-ből a tennivaló azonosítóját (ha van)
    // pl.: http://localhost:8080/tennivalo/1/?ajax=igen
    var currentUrl = $.url();
    tennivaloId = currentUrl.segment(2);

    // ha van id, akkor letöltjük a szerverről a tennivaló adatait és
    // megjelenítjük:
    if (tennivaloId) {
      tennivalotLetoltEsMegjelenit(tennivaloId);
    } else {
      // ha nincs id, akkor ez egy új tennivaló, tehát megjelenítjük a
      // létrehozó gombot:
      $('#letrehoz-gomb').show();
    }

    // eseménykezelők az egyes gombokhoz:
    $("#letrehoz-gomb").click(function(esemeny) {
      esemeny.preventDefault();
      modositasokatBekuld();
    });

    $("#modosit-gomb").click(function(esemeny) {
      esemeny.preventDefault();
      modositasokatBekuld();
    });

    $("#torol-gomb").click(function(esemeny) {
      esemeny.preventDefault();
      torlesiKerestBekuld();
    });

  });

  function tennivalotLetoltEsMegjelenit(tennivaloId) {
    $.ajax({
      url : '/tennivalo/' + tennivaloId + '/',
      dataType : 'json', // the data type we expect in the response
      method : 'GET',
      async : true,
      cache : false,
      timeout : 15000,

      success : function(tennivalo) {
        console.log('tennivalo: ' + tennivalo);

        // a letöltött tennivalót kirakjuk a form mezőibe:
        $('[name=id]').val(tennivalo.id);
        $('[name=fejlec]').val(tennivalo.fejlec);
        $('[name=leiras]').val(tennivalo.leiras);

        // majd megjelenítjük a módosít és a töröl gombokat:
        $('#modosit-gomb').show();
        $('#torol-gomb').show();
      },
      
      error : function(request, textStatus, errorThrown) {
        if (request.status == 404) {
          $('#nincsmegHiba').show();
        } else {
          $('#letoltesiHiba').show();
        }
      }
    });
  }

  function formMezokbolObjektumotKeszit() {
    return {
      "id" : $('[name=id]').val(),
      "fejlec" : $('[name=fejlec]').val(),
      "leiras" : $('[name=leiras]').val(),
    };
  }

  function modositasokatBekuld() {
    
    try {
      mezokKitoltottsegetEllenoriz();

      $.ajax({
        url : '/tennivalo/',
        dataType : 'json',
        method : tennivaloId != null ? 'PUT' : 'POST',
        async : true,
        cache : false,
        timeout : 15000,
        contentType : "application/json",
        data : JSON.stringify(formMezokbolObjektumotKeszit()),

        success : function(response) {
          // sikerült a módosítás, továbbküldjük a böngészőt a
          // nyitóoldalra:
          window.location.href = '/';
        },
        
        error : function(request, textStatus, errorThrown) {
          if (request.status == 404) {
            $('#nincsmegHiba').show();
          } else if (request.status == 409) {
            $('#keszetNemLehetModositaniHiba').show();
          } else {
            $('#mentesiHiba').show();
          }
        }
      });
      
    } catch (kivetel) {
      console.log(kivetel);
    }
  }

  function mezokKitoltottsegetEllenoriz() {
    
    var form = formMezokbolObjektumotKeszit();
    var rendbenVanEMind = true;

    $.each(['fejlec', 'leiras'], function(index, mezoNev) {
      
      try {
        mezoKitoltottsegetEllenoriz(form, mezoNev);
      } catch (mezoEllenorzesiHiba) {
        rendbenVanEMind = false;
      }
      
    });
    
    if (!rendbenVanEMind) {
      throw "a mezők kitöltöttsége hiányos";
    }

  }

  function mezoKitoltottsegetEllenoriz(form, mezoNev) {
    
    var mezo = $('[name=' + mezoNev + ']');
    
    if (form[mezoNev] == '') {
      mezo.addClass('mezoHiba');
      mezo.next('span').show();
      throw "A mezőt ki kell tölteni!"
    } else {
      mezo.removeClass('mezoHiba');
      mezo.next('span').hide();
    }
  }

  function torlesiKerestBekuld() {

    $.ajax({
      url : '/tennivalo/' + tennivaloId + '/',
      method : 'DELETE',
      async : true,
      cache : false,
      timeout : 15000,

      success : function(response) {
        // sikerült a törlés, továbbküldjük a böngészőt a nyitóoldalra:
        window.location.href = '/';
      },
      error : function(request, textStatus, errorThrown) {
        if (request.status == 404) {
          $('#nincsmegHiba').show();
        } else if (request.status == 409) {
          $('#keszetNemLehetModositaniHiba').show();
        } else {
          $('#torlesiHiba').show();
        }
      }
    });
  }

})();