package hu.vasvari.tennivalok;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Egy egyszerű böngésző-alapú teszt, Selenium WebDriver használatával.
 * 
 * A Selenium tesztek futtatásához előbb installálni kell a GeckoDriver-t
 * (Firefox-hoz):
 * 
 * Windows:
 * <ol>
 * <li>Letölteni az oldal aljáról a megfelelő Windows verziót:
 * https://github.com/mozilla/geckodriver/releases</li>
 * <li>Kitömöríteni, majd rárakni a PATH-ra.</li>
 * </ol>
 * 
 * Linux:
 * 
 * <pre>
 * wget https://github.com/mozilla/geckodriver/releases/download/v0.24.0/geckodriver-v0.24.0-linux64.tar.gz
 * tar xf geckodriver-v0.24.0-linux64.tar.gz
 * sudo cp geckodriver /usr/local/bin/
 * rm geckodriver
 * rm geckodriver*.gz
 * </pre>
 *
 * 
 * A teszt Eclipse-ből is futtatható: jobbegérgomb/Run as/JUnit test segítségével.
 * 
 * @author Richard
 *
 */
public class TennivalokApplicationTests {

  private WebDriver driver;

  // minden egyes teszt indítása előtt:
  // WebDriver inicializáció: elindítja a Firefox-ot:
  @Before
  public void initWebDriver() {
    driver = new FirefoxDriver();
    // minden elemre vár max 2 mp-et:
    driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
  }

  // minden teszt lefutása után:
  @After
  public void quitWebDriver() {
    // bezárja a böngészőablakot 
    driver.quit();
  }

  // néhány tesztadat, amit a tesztesetünk felhasznál:
  private String TESZTADAT_FEJLEC_SZOVEG = "Borfesztivál meglátogatása";
  private String TESZTADAT_LEIRAS_SZOVEG = "Eszem-iszom :)";
  private String TESZTADAT_LEIRAS_SZOVEG2 = "Eszem-iszom dínom dánom :)";

  /**
   * Egy nem optimalizált teszteset. 
   * 
   * Teszt-Forgatókönyv: tennivaló létrehozása, módosítása és törlése
   * ("szappanopera" teszt)
   */
  @Test
  public void tennivaloLetrehozasanakTesztje() throws InterruptedException {

    // Lépés:
    driver.get("http://localhost:8080");
    // Ellenőrzés:
    assertThat(driver.getTitle()).isEqualTo("Tennivalók");

    // Lépés:
    WebElement tennivalotLetrehozLink = driver.findElement(By.id("tennivalot-letrehoz"));
    tennivalotLetrehozLink.click();
    // Ellenőrzés:
    assertThat(driver.getTitle()).isEqualTo("Új Tennivaló");

    // Lépés:
    WebElement fejlec = driver.findElement(By.name("fejlec"));
    WebElement leiras = driver.findElement(By.name("leiras"));
    WebElement fontossag = driver.findElement(By.cssSelector("label[for='fontossag1']"));
    fejlec.sendKeys(TESZTADAT_FEJLEC_SZOVEG);
    leiras.sendKeys(TESZTADAT_LEIRAS_SZOVEG);
    fontossag.click();
    // Thread.sleep(5000);
    WebElement letrehoz = driver.findElement(By.cssSelector("button#letrehozas-gomb"));
    letrehoz.click();
    // Ellenőrzés:
    assertThat(driver.getTitle()).isEqualTo("Tennivalók");

    // Lépés:
    List<WebElement> tennivalok = driver.findElements(By.cssSelector("tbody tr"));
    tennivalotFejlecAlapjanKinyit(tennivalok, TESZTADAT_FEJLEC_SZOVEG);

    // Ellenőrzés:
    assertThat(driver.getTitle()).isEqualTo("Tennivaló Szerkesztése");
    fejlec = driver.findElement(By.name("fejlec"));
    leiras = driver.findElement(By.name("leiras"));
    assertThat(fejlec.getAttribute("value")).isEqualTo(TESZTADAT_FEJLEC_SZOVEG);
    assertThat(leiras.getAttribute("value")).isEqualTo(TESZTADAT_LEIRAS_SZOVEG);

    // Lépés:
    leiras.clear();
    leiras.sendKeys(TESZTADAT_LEIRAS_SZOVEG2);
    WebElement mentes = driver.findElement(By.cssSelector("button#mentes-gomb"));
    mentes.click();
    // Ellenőrzés:
    assertThat(driver.getTitle()).isEqualTo("Tennivalók");

    // Lépés:
    tennivalok = driver.findElements(By.cssSelector("tbody tr"));
    tennivalotFejlecAlapjanKinyit(tennivalok, TESZTADAT_FEJLEC_SZOVEG);
    // Ellenőrzés:
    assertThat(driver.getTitle()).isEqualTo("Tennivaló Szerkesztése");
    fejlec = driver.findElement(By.name("fejlec"));
    leiras = driver.findElement(By.name("leiras"));
    assertThat(fejlec.getAttribute("value")).isEqualTo(TESZTADAT_FEJLEC_SZOVEG);
    assertThat(leiras.getAttribute("value")).isEqualTo(TESZTADAT_LEIRAS_SZOVEG2);

    // Lépés:
    WebElement torles = driver.findElement(By.cssSelector("button#torles-gomb"));
    torles.click();
    // Ellenőrzés:
    assertThat(driver.getTitle()).isEqualTo("Tennivalók");

    // Lépés:
    tennivalok = driver.findElements(By.cssSelector("tbody tr"));
    assertThat(tennivalotFejlecAlapjanKinyit(tennivalok, TESZTADAT_FEJLEC_SZOVEG)).isFalse();
    assertThat(driver.getTitle()).isEqualTo("Tennivalók");
  }

  /**
   * Rákattint a megadott fejlécű tennivalóra.
   * 
   * @param tennivalok a tennivalók WebElementjeit tartalmazó lista
   * @param fejlec     amit keresünk
   * @return true, ha talált ilyen fejlécű tennivalót, false, ha nem.
   */
  private boolean tennivalotFejlecAlapjanKinyit(List<WebElement> tennivalok, String fejlec) {
    for (WebElement tennivalo : tennivalok) {
      WebElement link = tennivalo.findElement(By.cssSelector(".fejlec a"));
      if (TESZTADAT_FEJLEC_SZOVEG.equals(link.getText())) {
        link.click();
        return true;
      }
    }
    return false;
  }

}
